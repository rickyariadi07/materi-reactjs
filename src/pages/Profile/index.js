import React from 'react';
import { Link } from "react-router-dom";
import classes from './profile.module.css';

const Profile = () => {
  return (
    <div className={classes.Wrapper}>
      <h1>Ini Halaman Profile</h1>
      <Link to='/' className="header_text" >
          <strong>Ke Halaman Home</strong>
      </Link>
      <br/>
      <Link to='/login' className="header_text" >
          <strong>Ke Halaman Login</strong>
      </Link>
      <br/>
      <Link to='/dashboard' className="header_text" >
          <strong>Ke Halaman Dashboard</strong>
      </Link>
    </div>
  );
};

export default Profile;