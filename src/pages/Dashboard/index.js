import React from 'react';
import { Link } from "react-router-dom";
import classes from './dashboard.module.css';

const Dashboard = () => {
  return (
    <div className={classes.Wrapper}>
      <h1>Ini Halaman Dashboard</h1>
      <Link to='/' className="header_text" >
          <strong>Ke Halaman Home</strong>
      </Link>
      <br/>
      <Link to='/login' className="header_text" >
          <strong>Ke Halaman Login</strong>
      </Link>
      <br/>
      <Link to='/profile' className="header_text" >
          <strong>Ke Halaman Profile</strong>
      </Link>
    </div>
  );
};

export default Dashboard;