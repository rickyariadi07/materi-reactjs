import React from 'react';
import { Link } from "react-router-dom";
import classes from './home.module.css';

const Home = () => {
  return (
    <div className={classes.Wrapper}>
      <h1>Ini Halaman Home</h1>
      <Link to='/login' className="header_text" >
          <strong>Ke Halaman Login</strong>
      </Link>
      <br/>
      <Link to='/dashboard' className="header_text" >
          <strong>Ke Halaman Dashboard</strong>
      </Link>
      <br/>
      <Link to='/profile' className="header_text" >
          <strong>Ke Halaman Profile</strong>
      </Link>
    </div>
  );
};

export default Home;