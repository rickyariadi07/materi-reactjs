import React from 'react';
import { Link } from "react-router-dom";
import classes from './login.module.css';

const Login = () => {
  return (
    <div className={classes.Wrapper}>
      <h1>Login</h1>
      <Link to='/' className="header_text" >
          <strong>Ke Halaman Home</strong>
      </Link>
      <br/>
      <Link to='/dashboard' className="header_text" >
          <strong>Ke Halaman Dashboard</strong>
      </Link>
      <br/>
      <Link to='/profile' className="header_text" >
          <strong>Ke Halaman Profile</strong>
      </Link>
    </div>
  );
};

export default Login;