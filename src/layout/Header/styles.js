const styles = {
  header_text: (marginLeft, marginRight) => ({
      display: 'inline-block',
      position: 'relative',
      width: 127,
      height: 42,
      WebkitTransition: 'all .4s ease',
      transition: 'all .4s ease',
      fontSize: 15,
      padding: 8,
      textAlign: 'center',
      marginLeft: marginLeft,
      marginRight: marginRight
  }),  
  
  header_margin: {
      marginLeft: 30,
      marginRight: 30,
  },
  menu_margin: {
      display: 'flex',
      justifyContent: 'center',        
  },
  create_job: {
      marginTop: 7,
      background: '#37A000',
      fontWeight: 'bold',
      borderRadius: 10,
      display: 'block',
      width: '12%',
      marginRight: 30,
  },
  text_create_job:{
      verticalAlign: '-webkit-baseline-middle',
      fontSize: 16,
  },  
  ContainerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#F5F5F5',
    paddingLef: 20,
    paddingRight: 20,
  },
  
  logoImage: {
    resize: 'contain',
    height: 31,
    width: 150,
    marginRight: 10,
  },
  
  ContainerMid:{
    display: 'flex',
    flexDirection: 'row',
    padding:10,
    alignItems: 'center',
  },
  
  BtnSearch: {
    backgroundColor: 'green',
  },
}

export default styles;