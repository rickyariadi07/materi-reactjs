import React, {useState} from 'react';
import { Typography, Box, Grid, Button, Link } from '@material-ui/core'
import classes from './header.module.css';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { green } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    primary: green,
  },
});

const useStyles = makeStyles(theme => ({
  formControl: {    
    width: "4vw"
  },
  selectEmpty: {
    marginTop: theme.spacing(1)
  },
  select: {
    height: "1vh"
  },
  inputLabel: {
    fontSize: "4vh",
    alignSelf: "center"
  }
}));

const Header = () => {
  const dekor = useStyles();

  const [all, setAll] = useState('');

  const handleChange = (event) => {
    setAll(event.target.value);    
  };

  const preventDefault = (event) => event.preventDefault();

  return (
    <div>
      <Box className={classes.ContainerTop}>
        <Typography>WA: 0877 1168 1169 | Office: 021-89121288 | Email: info@edulancer.com</Typography>
        <Typography>WA: 0877 1168 1169 | Office: 021-89121288 | Email: info@edulancer.com</Typography>
      </Box>
      <Grid className={classes.ContainerMid}>
        <img className={classes.logoImage} src={require('../../assets/image/logo.png')} alt="Edulancer"/>
        <FormControl variant="outlined" className={dekor.formControl} size={'small'}>
          <InputLabel id="demo-simple-select-outlined-label">All</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={all}
            onChange={handleChange}
            label="All"                      
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>  
        </FormControl> 
        <FormControl>
          <TextField id="outlined-basic" label="Outlined" variant="outlined" size={'small'} />
        </FormControl> 
        <ThemeProvider theme={theme}>
          <Button variant="contained" color="primary">
            Search
          </Button> 
        </ThemeProvider>
      </Grid>
      <Grid 
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
        className={classes.bottomMenu}>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
           DESAIN WEB
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          DESAIN GRAFIS
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          PEMROGRAMAN
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          PEMASARAN ONLINE
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          PENULISAN
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          PENERJEMAHAN
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          AUDIO & VIDEO
          </Link>
        </Typography>
        <Typography fontWeight="fontWeightBold" variant={'body1'} className={classes.menuText}> 
          <Link href="#" onClick={preventDefault} color="inherit">
          KURSUS
          </Link>
        </Typography>       
      </Grid>
    </div>
  );
};

export default Header;