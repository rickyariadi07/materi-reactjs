import React from 'react';
import { Switch, Route } from "react-router-dom";
import Home from '../../pages/Home';
import Login from '../../pages/Login';
import Dashboard from '../../pages/Dashboard';
import Profile from '../../pages/Profile';


const Main = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/profile" component={Profile} />
      <Route exact path="/dashboard" component={Dashboard} />
    </Switch>
  );
};

export default Main;