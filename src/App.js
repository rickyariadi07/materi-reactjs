import React from 'react';
import Content from './layout/Content';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>  
      <Content />
    </BrowserRouter>
  );
}

export default App;
